// Carousel A
$('.owl-carousel').owlCarousel({
  autoplay: true,
  loop:true,
  autoplayTimeout: 5000,
  navigation: true,
  margin:8,
  responsive: {
    100: {
      items: 1
    },
    992: {
      items: 3
    }
    ,
    1200: {
      items: 4
    }
  }
})

// Carousel B
$('.owl-carousel-b').owlCarousel({
  autoplay: true,
  loop:true,
  autoplayTimeout: 115500,
  navigation: false,
  margin:0,
  responsive: {
    100: {
      items: 1
    },
    992: {
      items: 3
    }
    ,
    1200: {
      items: 3
    }
  }
})


// Royal Slider
jQuery(document).ready(function($) {
  $('#full-width-slider').royalSlider({
    arrowsNav: true,
    loop: false,
    keyboardNavEnabled: true,
    controlsInside: false,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true, 
    autoScaleSliderWidth: 960,     
    autoScaleSliderHeight: 350,
    controlNavigation: 'bullets',
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: false,
    transitionType:'move',
    globalCaption: false,
    deeplinking: {
    enabled: true,
    change: false
    },
    imgWidth: 1440,
    imgHeight: 376,
    autoPlay: {
    enabled: true,
    pauseOnHover: true
      }
  });
});

feather.replace()

$(document).ready(function(){

    $(document).on('click','.departments-menu-item .departments-menu-link',function(){
        var active_div = $(this).next();
        if ($(window).width() > 991) {
            $(this).next().addClass("active");
            $(document).find('.departments-menu-item-row').not(active_div).removeClass('active');
            $(this).addClass('active');
            $(document).find(".departments-menu-item .departments-menu-link").not(this).removeClass('active');
        }
    });
    $(document).on('click','.departments-menu-link',function(){
        if (!$(this).next().hasClass('show')) {
            $('.departments-menu-item-row').removeClass('show');
            $(this).next().addClass('show');
        }
        else if ($(this).next().hasClass('show')) {
            $(this).next().removeClass('show');
        }
        $(this).addClass('show');
        $(document).find(".departments-menu-link").not(this).removeClass('show');
        return false;
    });
    $(document).on("click", ".fashion-row", function(){
        return false;	
    });
    $(document).on('click' ,'.navbar-toggler', function(){
        $('.mega-menu-toggle-bar').toggleClass('fa-times');
        $('.mega-menu-toggle-bar').toggleClass('fa-bars');
    });
});




 
